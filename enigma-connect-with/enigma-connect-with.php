<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://enigma-chris.ca
 * @since             1.0.0
 * @package           Follow_Me
 *
 * @wordpress-plugin
 * Plugin Name:       Enigma Connect with
 * Plugin URI:        https://enigma-chris.ca
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           0.0.1
 * Author:            Christian
 * Author URI:        https://enigma-chris.ca
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       enigma_connect_with
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if(file_exists($filename = dirname(__FILE__).'/vendor/autoload.php')){
	require_once  dirname(__FILE__).'/vendor/autoload.php';
}

use Inc\Base\Activate;
use Inc\Base\Deactivate;

define( 'PLUGIN_NAME_VERSION', '0.0.1' );

		if(phpversion() < "7"){
		echo	'<div class="error notice">
			    <p> You are currently running php version '.phpversion().' You need a php version 7 or above to run this plugin</p>
			</div>';
		exit;
		}else{
			function activate_enigma_plugin(){
				Activate::activate();
			}
			register_activation_hook( __FILE__, $function = 'activate_enigma_plugin');
		}
function deactivate_enigma_plugin(){
	 Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, $function = 'deactivate_enigma_plugin' );

require_once  dirname(__FILE__).'/LoadWidget.php';
$t = new LoadWidget();

if(class_exists('Inc\\Bootstrap\\Init')){
		Inc\Bootstrap\Init::register_services();
}
