<?php
/**
 *
 * This class registers all the class services that we used during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Connect_With
 * @subpackage Enigma_ChrisIncludes/Bootstrap
 * @author     Christian <nwachukwu16@gmail.com>
 */

namespace Inc\Bootstrap;

use Inc\Api\Shortcodes\SocialShortCode;
use Inc\Api\Settings\Activate\ActivatePageSettings\ActivatePageSettings;
use Inc\Base\Enqueue;


final class Init
{
    public static function get_services(){
          return  [
                      SocialShortCode::class,
                      ActivatePageSettings::class,
                      Enqueue::class,
                  ];
    }

    public static function register_services(){

            foreach(self::get_services() as $class ){
                $service = self::instantiate($class);
                if(method_exists($service, 'register')){
                   $service->register();
                }

            }
    }

 private static function instantiate($class){
   return new $class();
 }



















}
