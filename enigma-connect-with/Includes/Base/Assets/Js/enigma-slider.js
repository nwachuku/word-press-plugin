$( function() {

  $( function() {

    var size = WP_SLIDER.icon_size;
   $( "#slider-range-max" ).slider({
           range: "max",
           min: 15,
           max: 500,
           value: size,
           slide: function( event, ui ) {
             jQuery('#enigma_icon_size').val(ui.value );
             size =  ui.value+"px";
             jQuery('.social-icon').css('fontSize', size);
           }
   });
 } );

 $( function() {

   var Titlesize = WP_SLIDER.title_size;
  $( "#title-slider-range-max" ).slider({
          range: "max",
          min: 15,
          max: 500,
          value: Titlesize,
          slide: function( event, ui ) {
            jQuery('#enigma_title_size').val(ui.value );
            Titlesize =  ui.value+"px";
            jQuery('.icon-title').css('fontSize', Titlesize);
          }
  });

} );


  } );
