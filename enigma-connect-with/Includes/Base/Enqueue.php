<?php
/**
 *
 * This class loads all the script and styles during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Connect_With
 * @subpackage Enigma_ChrisIncludes/Base
 * @author     Christian <nwachukwu16@gmail.com>
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{

      //register the scripts
      public  function register(){
        //Add a plugin admin page to the wordpresss dashboard
        add_action( 'admin_enqueue_scripts', [$this, 'enqueue']);
        //Add a script in the front-end
        add_action( 'wp_enqueue_scripts', [$this, 'add_theme_scripts'] );

      }

      //Enqueue the script that we need for the plugin
      function enqueue(){

      	  global $pagenow, $typenow,$post_type ;

          //If we are in the admin page, load these scripts
          if('admin.php' == $pagenow){

            if( is_admin() ) {

               // Add the color picker css file
               wp_enqueue_style( 'wp-color-picker' );
               // Include our custom jQuery file with WordPress Color Picker dependency
               wp_enqueue_script( 'color-picker-handle', plugins_url( '/Assets/Js/enigma-color-picker.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
               wp_enqueue_script( 'jquery-ui-slider-handle', plugins_url( '/Assets/Js/enigma-slider.js', __FILE__ ),  ['jquery', 'jquery-ui-slider'], false, true );
               wp_enqueue_style( $handle = 'back-end-style', $src = $this->plugin_url.'Assets/Css/back-end.css' );
               wp_enqueue_style( $handle   = 'font-awesome',  $src = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
               //Important for running ajax request.
       	       wp_localize_script($handle = 'jquery-ui-slider-handle',  'WP_SLIDER', [
       		                         'icon_size' => get_option('enigma_icon_size' ),
                                   'title_size' => get_option('enigma_title_size' ),
       	           ]);
           }

           //Load other scripts
          wp_enqueue_style( $handle   = 'bootstrap_css', $src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
          wp_enqueue_script( $handle  = 'jquery_file',   $src ='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
          wp_enqueue_script( $handle  = 'bootstrap_js',  $src ='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
          wp_enqueue_style( $handle   = 'jquery-ui-css', $src = 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css');
          wp_enqueue_style( $handle   = 'jquery-ui-js',  $src = 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js');
          wp_enqueue_script( $handle  = 'bootstrap_js',  $src ='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');

          }
      }

      //function to enqueue the front-end scripts
      function add_theme_scripts() {
        wp_enqueue_style( $handle   = 'font-awesome',  $src = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style( $handle = 'social-style', $src = $this->plugin_url.'Assets/Css/style.css' );

      }
}
