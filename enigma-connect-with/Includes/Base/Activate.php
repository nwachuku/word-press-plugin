<?php
/**
 *
 * This class defines all paths during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Connect_With
 * @subpackage Enigma_Chris/Includes
 * @author     Christian <nwachukwu16@gmail.com>
 */

namespace Inc\Base;

class Activate
{

  	public static function activate() {
  		//flush rewrite rules
  		flush_rewrite_rules();
  	}

  	function register(){

  	}
    
}
