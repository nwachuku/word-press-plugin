<?php

/**
 *
 * This class defines all paths during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Enigma_Connect_With
 * @subpackage Enigma_ChrisIncludes/Base
 * @author     Christian <nwachukwu16@gmail.com>
 */

 namespace Inc\Base;

class Deactivate
{

	public static function deactivate() {
		flush_rewrite_rules();
	}


}
