<?php
/**
 *
 * This class defines all paths during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Connect_With
 * @subpackage Enigma_ChrisIncludes/Base/BaseController.php
 * @author     Christian <nwachukwu16@gmail.com>
 */

namespace Inc\Base;

class BaseController{

  //Setting up the plugin path variables
  public $plugin_path;
  public $plugin_url;
  public $plugin;
  public $plugin_version;

  //Setting up the paths when the constructor has been initialzed
  public function __construct(){
      $this->plugin_path = plugin_dir_path( dirname(__FILE__, 2) );
      $this->plugin_url   = plugin_dir_url( __FILE__ ,2);

      $path = explode($separator = '/', plugin_basename( __FILE__ ,1) );
      $this->plugin  =  $path[0].'/'.$path[0].'.php';
      $this->plugin_version = '2.0.0';
  }


}
