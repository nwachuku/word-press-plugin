<?php
/**
 * Created by PhpStorm.
 * User: uchenna
 * Date: 12/30/2017
 * Time: 3:57 PM
 */


namespace Inc\Api\Shortcodes;
use WP_Query;

class SocialShortCode {

	public function register()
	{
		add_shortcode('sample_shortcode', [$this, 'enigma_sample_shortcode']);
	}

	public  function enigma_sample_shortcode($atts, $content = null){

	?>

	<style>
			div.social-plugin a.social-link{
				text-decoration: none !important;
				font-size: <?php echo get_option('enigma_icon_size' )."px;"; ?>  !important;
				margin-right: 7px;
				border-bottom: none;
			}

			div.social-plugin .icon-title{
				font-size: <?php echo esc_attr( get_option( $option = 'enigma_title_size', $default = false ) )."px;" ?>
			}
	</style>

	<?php


		//defines the attributes that the short code accepts
		$atts = shortcode_atts([
			'title'     => '<h4 class="icon-title" style="color:'.get_option( $option = 'enigma_title_color').'" >'.__('Connect with me on',"enigma_connect_with").'</h4>',
			'facebook'  => '',
			'linkedin'  => '',
			'instagram' => '',
			'googleplus'		=> '',
			'pinterest'	=> '',
			'flicker'		=> '',
			'youtube'		=> '',
			'twitter'   => '',
			'all' 			=> ''
		], $atts, 'sample_shortcode')	;

		$response = '<h4 class="icon-title" style="color:'.get_option( $option = 'enigma_title_color').'" >'.__($atts['title'], "enigma_connect_with").'</h4>';

		if( 'show' == $atts['all'] ){

					if(!empty(get_option( $option = 'enigma_facebook_url'))){
						$facebook_color = get_option( $option = 'enigma_facebook_color');
						$response.= ' <a class="social-link" style="color:'.$facebook_color.'" href="'.get_option( $option = 'enigma_facebook_url').'"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>';
					}

					if(!empty(get_option( $option = 'enigma_linkedin_url'))){
						$linkedin_color = get_option( $option = 'enigma_linkedin_color');
						$response.= ' <a class="social-link" style="color:'.$linkedin_color.'" href="'.get_option( $option = 'enigma_linkedin_url').'"> <i class="fa fa-linkedin" aria-hidden="true"></i></a>';
					}

					if(!empty(get_option( $option = 'enigma_instagram_url'))){
						$instagram_color = get_option( $option = 'enigma_instagram_color');
						$response.= ' <a class="social-link" style="color:'.$instagram_color.'" href="'.get_option( $option = 'enigma_instagram_url').'"> <i class="fa fa-instagram" aria-hidden="true"></i> </a>';
					}

					if(!empty(get_option( $option = 'enigma_google_plus_url'))){
						$google_plus_color = get_option( $option = 'enigma_google_plus_color');
						$response.= ' <a class="social-link" style="color:'.$google_plus_color.'" href="'.get_option( $option = 'enigma_google_plus_url').'"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a>';
					}

					if(!empty(get_option( $option = 'enigma_pinterest_url'))){
						$pinterest_color = get_option( $option = 'enigma_pinterest_color');
						$response.= ' <a class="social-link" style="color:'.$pinterest_color.'" href="'.get_option( $option = 'enigma_pinterest_url').'"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a>';
					}

					if(!empty(get_option( $option = 'enigma_flicker_url'))){
						$flicker_color = get_option( $option = 'enigma_flicker_color');
						$response.= ' <a class="social-link" style="color:'.$flicker_color.'" href="'.get_option( $option = 'enigma_flicker_url').'"> <i class="fa fa-flickr" aria-hidden="true"></i> </a>';
					}

					if(!empty(get_option( $option = 'enigma_twitter_url'))){
						$twitter_color = get_option( $option = 'enigma_twitter_color');
						$response.= ' <a class="social-link" style="color:'.$twitter_color.'" href="'.get_option( $option = 'enigma_twitter_url').'">  <i class="fa fa-twitter" aria-hidden="true"></i> </a>';
					}

					if(!empty(get_option( $option = 'enigma_youtube_url'))){
						$youtube_color = get_option( $option = 'enigma_youtube_color');
						$response.= ' <a class="social-link" style="color:'.$youtube_color.'"  href="'.get_option( $option = 'enigma_youtube_url').'">  <i class="fa fa-youtube" aria-hidden="true"></i> </a>';
					}
					return  '<div class="social-plugin">'.$response. '</div>';
		}

		if( 'show' == $atts['facebook'] ){
				 $response.= '<a class="social-link"  style="color:'.get_option( $option = 'enigma_facebook_color').'"  href="'.get_option( $option = 'enigma_facebook_url').'"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>';
		}
		if( 'show' == $atts['linkedin'] ){
				 $response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_linkedin_color').'" href="'.get_option( $option = 'enigma_linkedin_url').'"> <i class="fa fa-linkedin" aria-hidden="true"></i></a>';
		}
		if( 'show' == $atts['instagram'] ){
				 $response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_instagram_color').'" href="'.get_option( $option = 'enigma_instagram_url').'"> <i class="fa fa-instagram" aria-hidden="true"></i>';
		}
		if( 'show' == $atts['googleplus'] ){
				 $response.= ' <a class="social-link" style="color:'.get_option( $option = 'enigma_google_plus_color').'" href="'.get_option( $option = 'enigma_google_plus_url').'"> <i class="fa fa-google-plus" aria-hidden="true"></i> ';
		}

		if( 'show' == $atts['pinterest'] ){
				 $response.= ' <a class="social-link" style="color:'.get_option( $option = 'enigma_pinterest_color').'" href="'.get_option( $option = 'enigma_pinterest_url').'"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a>';
		}
		if( 'show' == $atts['flicker'] ){
				 $response.= ' <a class="social-link" style="color:'.get_option( $option = 'enigma_flicker_color').'" href="'.get_option( $option = 'enigma_flicker_url').'"> <i class="fa fa-flickr" aria-hidden="true"></i> </a>';
		}
		if( 'show' == $atts['twitter'] ){
				 $response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_twitter_color').'" href="'.get_option( $option = 'enigma_twitter_url').'"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>';
		}
		if( 'show' == $atts['youtube'] ){
					$youtube_color = get_option( $option = 'enigma_youtube_color');
				 $response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_youtube_color').'" href="'.get_option( $option = 'enigma_youtube_url').'"> <i class="fa fa-youtube" aria-hidden="true"></i> </a>';
		}


		return '<div class="social-plugin">'.$response. '</div>';
	}

}
