<?php

/**
 * Fired during plugin activation
 *
 * @link       https://enigma-chris.ca
 * @since      1.0.0
 *
 * @package    Enigma_Chris
 * @subpackage Enigma_Chris/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Chris
 * @subpackage Enigma_Chris/includes
 * @author     Christian <nwachukwu16@gmail.com>
 */

namespace Inc\Api\Callbacks;
use \Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
      public function adminDashboard(){
        return require_once("$this->plugin_path/Templates/connect_settings.php");
      }

      public function adminColor(){
        return require_once("$this->plugin_path/Templates/connect_color_settings.php");
      }

      public function adminIonSize(){
        return require_once("$this->plugin_path/Templates/connect_size_settings.php");
      }

     public function enigmaOptionGroup($input){
       return $input;
     }

      public function enigmaSocialColorOptions($input){
        return $input;
      }

      public function enigmaIconSizeOptions($input){
        return $input;
      }


     public function enigmaAdminSection(){
      echo 'Our section';
     }

     public function enigmaFacebook(){

       $value = esc_attr( get_option( $option = 'enigma_facebook_url', $default = false ) );
      echo '<input type="text" id="enigma_facebook_url" class="regular-text"  name="enigma_facebook_url" value = "'.$value.'"  placeholder="'.__("Facebook URL", "enigma_connect_with").'"/>';
     }
     public function enigmaSocialOptions($input){
       return $input;
     }
     public function enigmaLinkedIn(){
       $value = esc_attr( get_option( $option = 'enigma_linkedin_url', $default = false ) );
       echo '<input type="text" id="enigma_linkedin_url" class="regular-text"  name="enigma_linkedin_url" value = "'.$value.'"  placeholder="'.__("LinkedIn URL", "enigma_connect_with").'"/>';
     }

    public function enigmaInstagram(){
      $value = esc_attr( get_option( $option = 'enigma_instagram_url', $default = false ) );
      echo '<input type="text" id="enigma_instagram_url" class="regular-text"  name="enigma_instagram_url" value = "'.$value.'"  placeholder="'.__("Instagram URL", "enigma_connect_with").'"/>';
    }

    public function enigmaGooglePlus(){
      $value = esc_attr( get_option( $option = 'enigma_google_plus_url', $default = false ) );
      echo '<input type="text" id="enigma_google_plus_url" class="regular-text"  name="enigma_google_plus_url" value = "'.$value.'"  placeholder="'.__("Google+ URL", "enigma_connect_with").'"/>';

      }
    public function enigmaPinterest(){
      $value = esc_attr( get_option( $option = 'enigma_pinterest_url', $default = false ) );
      echo '<input type="text" id="enigma_pinterest_url" class="regular-text"  name="enigma_pinterest_url" value = "'.$value.'"  placeholder="'.__("Pinterest URL", "enigma_connect_with").'"/>';
      }
    public function enigmaFlicker(){
      $value = esc_attr( get_option( $option = 'enigma_flicker_url', $default = false ) );
      echo '<input type="text" id="enigma_flicker_url" class="regular-text"  name="enigma_flicker_url" value = "'.$value.'"  placeholder="'.__("Flicker URL", "enigma_connect_with").'"/>';
    }
    public function enigmaYoutube(){
      $value = esc_attr( get_option( $option = 'enigma_youtube_url', $default = false ) );
      echo '<input type="text" id="enigma_youtube_url" class="regular-text"  name="enigma_youtube_url" value = "'.$value.'"  placeholder="'.__("Youtube URL", "enigma_connect_with").'"/>';
    }
    public function enigmaTwitter(){
      $value = esc_attr( get_option( $option = 'enigma_twitter_url', $default = false ) );
      echo '<input type="text" id="enigma_twitter_url" class="regular-text"  name="enigma_twitter_url" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
    }



    //COLOR SETTINGS
    public function enigmaTitleColor(){
      $value = esc_attr( get_option( $option = 'enigma_title_color', $default = false ) );
      echo '<input type="text" id="enigma_title_color" class="regular-text color-field"  name="enigma_title_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
    }
    public function enigmaFacebookColor(){
      $value = esc_attr( get_option( $option = 'enigma_facebook_color', $default = false ) );
      echo '<input type="text" id="enigma_facebook_color" class="regular-text color-field"  name="enigma_facebook_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
    }
    public function   enigmaLinkedInColor(){
        $value = esc_attr( get_option( $option = 'enigma_linkedin_color', $default = false ) );
        echo '<input type="text" id="enigma_linkedin_color" class="regular-text color-field"  name="enigma_linkedin_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
      }
  public function   enigmaInstagramColor(){
      $value = esc_attr( get_option( $option = 'enigma_instagram_color', $default = false ) );
      echo '<input type="text" id="enigma_instagram_color" class="regular-text color-field"  name="enigma_instagram_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
    }
    public function   enigmaGoogleplusColor(){
        $value = esc_attr( get_option( $option = 'enigma_google_plus_color', $default = false ) );
        echo '<input type="text" id="enigma_google_plus_color" class="regular-text color-field"  name="enigma_google_plus_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
      }

      public function   enigmaPinterestColor(){
          $value = esc_attr( get_option( $option = 'enigma_pinterest_color', $default = false ) );
          echo '<input type="text" id="enigma_pinterest_color" class="regular-text color-field"  name="enigma_pinterest_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
        }

        public function   enigmaFlickerColor(){
          $value = esc_attr( get_option( $option = 'enigma_flicker_color', $default = false ) );
          echo '<input type="text" id="enigma_flicker_color" class="regular-text color-field"  name="enigma_flicker_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
        }

        public function   enigmaYoutubeColor(){
          $value = esc_attr( get_option( $option = 'enigma_youtube_color', $default = false ) );
          echo '<input type="text" id="enigma_youtube_color" class="regular-text color-field"  name="enigma_youtube_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
        }

      public function enigmaTwitterColor(){
        $value = esc_attr( get_option( $option = 'enigma_twitter_color', $default = false ) );
        echo '<input type="text" id="enigma_twitter_color" class="regular-text color-field"  name="enigma_twitter_color" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';
      }

      public function enigmaIconSize(){
        echo '<div id="slider-range-max"></div><br/>';
        $value = esc_attr( get_option( $option = 'enigma_icon_size', $default = false ) );
        $title_value = esc_attr( get_option( $option = 'enigma_title_size', $default = false ) );
        echo '<input type="hidden" id="enigma_icon_size" class="regular-text "  name="enigma_icon_size" value = "'.$value.'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';

        echo '<input type="hidden" id="enigma_title_size" class="regular-text"  name="enigma_title_size" value = "'.$title_value .'"  placeholder="'.__("Twitter URL", "enigma_connect_with").'"/>';

        echo '
        <h3 class="icon-title" style="font-size: '.$title_value.'px; color: '.get_option( $option = 'enigma_title_color').'">Sample Title</h3>
        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_facebook_color').'"> <i class="fa fa-facebook" aria-hidden="true"></i> </span>
        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_linkedin_color').'"> <i class="fa fa-linkedin" aria-hidden="true"></i> </span>
        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_instagram_color').'"> <i class="fa fa-instagram" aria-hidden="true"></i> </span>

        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_google_plus_color').'"> <i class="fa fa-google-plus" aria-hidden="true"></i> </span>
        <span class="social-icon" style="font-size: '.$value.'px ; color: '.get_option( $option = 'enigma_pinterest_color').'"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </span>
        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_flicker_color').'"> <i class="fa fa-flickr" aria-hidden="true"></i> </span>

        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_twitter_color').'"> <i class="fa fa-twitter" aria-hidden="true"></i> </span>
        <span class="social-icon" style="font-size: '.$value.'px; color: '.get_option( $option = 'enigma_youtube_color').'"> <i class="fa fa-youtube" aria-hidden="true"></i> </span>

        ';
      }

      public function  enigmaTitleSize(){
        echo '<div id="title-slider-range-max"></div><br/>';
      }


}
