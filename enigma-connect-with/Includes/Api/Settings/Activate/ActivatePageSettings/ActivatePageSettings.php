<?php

/**
 * Fired during plugin activation
 *
 * @link       https://enigma-chris.ca
 * @since      1.0.0
 *
 * @package    Enigma_Chris
 * @subpackage Enigma_Chris/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Chris
 * @subpackage Enigma_Chris/includes
 * @author     Christian <nwachukwu16@gmail.com>
 */

namespace   Inc\Api\Settings\Activate\ActivatePageSettings;

use \Inc\Api\Settings\SettingsApi;
use \Inc\Api\Callbacks\AdminCallbacks;


class ActivatePageSettings
{

	public $settings;

	public $pages;

	public $subpages;

	public $admin;


	public function __construct(){

	        // Initialize the class
		    $this->settings =   new SettingsApi();
				$this->admin    =   new AdminCallbacks();


	}

    public  function register(){

	    //call the setpages method
	    $this->setPages();

	    //call the setsubpages method
	    $this->setSubPages();

	    $this->setSections();

	    $this->setFields();

	    $this->setSettings();

	    //Add a plugin admin page to the wordpres dashboard
	    $this->settings->addPages($this->pages)
	                   ->withSubPage(__("Dashboard", "enigma_connect_with"))
	                   ->addSubPages(  $this->subpages )
	                   ->register();

    }

	private function setPages(){
		//Add the List of pages
		$this->pages    = [
			[
				'page_title'  => __("Socials settings", "enigma_connect_with"),
				'menu_title'  => __("Socials settings", "enigma_connect_with"),
				'capability'  => 'manage_options',
				'menu_slug'   =>  'enigma_connect_with_me',
				'callback'    => [$this->admin, 'adminDashboard'],
				'icon_url'    => 'dashicons-share',
				'position'    => 110
			],
		];
	}

	private function setSubPages(){
		//Add the List of pages
		$this->subpages    = [

														[
															'parent_slug' =>  'enigma_connect_with_me',
															'page_title'  =>  __("Color Settings", "enigma_connect_with"),
															'menu_title' 	=>  __("Color Setup", "enigma_connect_with"),
															'capability'  => 'manage_options',
															'menu_slug'   => 'enigma_color_set_up',
															'callback'    => [$this->admin, 'adminColor'],
														],
														[
															'parent_slug' => 'enigma_connect_with_me',
															'page_title'  => __("Icon Size", "enigma_connect_with"),
															'menu_title' =>  __("Icon Size", "enigma_connect_with"),
															'capability'   => 'manage_options',
															'menu_slug'  => 'enigma_icon_size',
															'callback'     => [$this->admin, 'adminIonSize'],
														],
												];
	}


	private function setSettings(){

		$args = [
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_facebook_url',
									'callback'        =>[$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_linkedin_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_instagram_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_google_plus_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_pinterest_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_flicker_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_youtube_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_settings',
									'option_name' => 'enigma_twitter_url',
									'callback'        => [$this->admin, 'enigmaSocialOptions']
								],
								//Color Settings
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_title_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_twitter_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_linkedin_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_instagram_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_google_plus_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_pinterest_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_flicker_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_youtube_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_color_settings',
									'option_name' => 'enigma_facebook_color',
									'callback'        => [$this->admin, 'enigmaSocialColorOptions']
								],

								//Icon DivisionByZeroError
								[
									'option_group' => 'enigma_connect_with_me_size_settings',
									'option_name' => 'enigma_icon_size',
									'callback'        => [$this->admin, 'enigmaIconSizeOptions']
								],
								[
									'option_group' => 'enigma_connect_with_me_size_settings',
									'option_name' => 'enigma_title_size',
									'callback'        => [$this->admin, 'enigmaIconSizeOptions']
								],


		];

		$this->settings->setSettings($args);
	}


	private function setSections(){

		$args = [
								[
									'id' 			 => 'enigma_admin_index',
								  'title' 	 => '', //title of the section
									'callback' => '',
									'page' 		 => 'enigma_connect_with_me' //page the section will appear
								],

								[
									'id' 			 => 'enigma_social_color_section',
								  'title' 	 => '', //title of the section
									'callback' => '',
									'page' 		 => 'enigma_color_set_up' //page the section will appear
								],
								[
									'id' 			 => 'enigma_icon_size_section',
								  'title' 	 => '', //title of the section
									'callback' => '',
									'page' 		 => 'enigma_icon_size' //page the section will appear
								],

						];

		$this->settings->setSections($args);
	}

	private function setFields(){

		$args = [
										[
										 'id' 				 => 'enigma_title_size', //field id
										 'title' 		   => __('Social Title', "enigma_connect_with"), //field title
										 'callback'    =>[$this->admin, 'enigmaTitleSize'],//function to show the field
										 'page'    	   => 'enigma_icon_size',//page the field will show
										 'section' 	   => 'enigma_icon_size_section',//section the field will appear
										 'args' 			 => [
																			 'label_for'  => 'cpt_manager',
																			 'class'      => 'ui-toggle',
																		 ]
									 ],
									[
									 'id' 				 => 'enigmaIconSize', //field id
									 'title' 		   => __('Set Icon Size', "enigma_connect_with"), //field title
									 'callback'    =>[$this->admin, 'enigmaIconSize'],//function to show the field
									 'page'    	   => 'enigma_icon_size',//page the field will show
									 'section' 	   => 'enigma_icon_size_section',//section the field will appear
									 'args' 			 => [
																		 'label_for'  => 'cpt_manager',
																		 'class'      => 'ui-toggle',
																	 ]
								 ],

								//Color Fields
								[
								 'id' 				 => 'enigma_title_color', //field id
								 'title' 		   => __('Tite Text', "enigma_connect_with"), //field title
								 'callback'    =>[$this->admin, 'enigmaTitleColor'],//function to show the field
								 'page'    	   => 'enigma_color_set_up',//page the field will show
								 'section' 	   => 'enigma_social_color_section',//section the field will appear
								 'args' 			 => [
																	 'label_for'  => 'cpt_manager',
																	 'class'      => 'ui-toggle',
																 ]
							 ],
								[
								 'id' 				 => 'enigma_facebook_color', //field id
								 'title' 		   => __('Facebook', "enigma_connect_with"), //field title
								 'callback'    =>[$this->admin, 'enigmaFacebookColor'],//function to show the field
								 'page'    	   => 'enigma_color_set_up',//page the field will show
								 'section' 	   => 'enigma_social_color_section',//section the field will appear
								 'args' 			 => [
																	 'label_for'  => 'cpt_manager',
																	 'class'      => 'ui-toggle',
																 ]
							 ],
							 [
								 'id' 				 => 'enigma_linkedin_color', //field id
								 'title' 		   => __('LinkedIn',"enigma_connect_with"), //field title
								 'callback'    =>[$this->admin, 'enigmaLinkedInColor'],//function to show the field
								 'page'    	   => 'enigma_color_set_up',//page the field will show
								 'section' 	   => 'enigma_social_color_section',//section the field will appear
								 'args' 			 => [
																	 'label_for'  => 'cpt_manager',
																	 'class'      => 'ui-toggle',
																 ]
							 ],
							  [
									'id' 				 => 'enigma_instagram_color', //field id
									'title' 		   => __('Instagram', "enigma_connect_with"), //field title
									'callback'    =>[$this->admin, 'enigmaInstagramColor'],//function to show the field
									'page'    	   => 'enigma_color_set_up',//page the field will show
									'section' 	   => 'enigma_social_color_section',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								[
									'id' 				 => 'enigma_google_plus_color', //field id
									'title' 		   => __('Google+',"enigma_connect_with"), //field title
									'callback'    =>[$this->admin, 'enigmaGooglePlusColor'],//function to show the field
									'page'    	   => 'enigma_color_set_up',//page the field will show
									'section' 	   => 'enigma_social_color_section',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								[
									'id' 				 => 'enigma_pinterest_color', //field id
									'title' 		   => __('Pinterest', "enigma_connect_with"), //field title
									'callback'    =>[$this->admin, 'enigmaPinterestColor'],//function to show the field
									'page'    	   => 'enigma_color_set_up',//page the field will show
									'section' 	   => 'enigma_social_color_section',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								[
									'id' 				 => 'enigma_flicker_color', //field id
									'title' 		   => __('Flicker', "enigma_connect_with"), //field title
									'callback'    =>[$this->admin, 'enigmaFlickerColor'],//function to show the field
									'page'    	   => 'enigma_color_set_up',//page the field will show
									'section' 	   => 'enigma_social_color_section',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								[
									'id' 				 => 'enigma_youtube_color', //field id
									'title' 		 => __('Youtube', "enigma_connect_with"), //field title
									'callback'   =>[$this->admin, 'enigmaYoutubeColor'],//function to show the field
									'page'    	 => 'enigma_color_set_up',//page the field will show
									'section' 	 => 'enigma_social_color_section',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								[
								 'id' 				 => 'enigma_twitter_color', //field id
								 'title' 		 => __('Twitter',"enigma_connect_with"), //field title
								 'callback'   =>[$this->admin, 'enigmaTwitterColor'],//function to show the field
								 'page'    	 => 'enigma_color_set_up',//page the field will show
								 'section' 	 => 'enigma_social_color_section',//section the field will appear
								 'args' 			 => [
																	 'label_for'  => 'cpt_manager',
																	 'class'      => 'ui-toggle',
																 ]
							 ],
								[
								  'id' 				 => 'enigma_facebook_url', //field id
									'title' 		 => __('Facebook', "enigma_connect_with"), //field title
									'callback'   =>[$this->admin, 'enigmaFacebook'],//function to show the field
									'page'    	 => 'enigma_connect_with_me',//page the field will show
									'section' 	 => 'enigma_admin_index',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								[
								  'id' 				 => 'enigma_linkedin_url', //field id
									'title' 		 => __('LinkedIn',"enigma_connect_with"), //field title
									'callback'   =>[$this->admin, 'enigmaLinkedIn'],//function to show the field
									'page'    	 => 'enigma_connect_with_me',//page the field will show
									'section' 	 => 'enigma_admin_index',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
																	]
								],
								 [
								   'id' 				 => 'enigma_instagram_url', //field id
								 	'title' 		 => __('Instagram', "enigma_connect_with"), //field title
								 	'callback'   =>[$this->admin, 'enigmaInstagram'],//function to show the field
									'page'    	 => 'enigma_connect_with_me',//page the field will show
								 	'section' 	 => 'enigma_admin_index',//section the field will appear
								 	'args' 			 => [
								 										'label_for'  => 'cpt_manager',
								 										'class'      => 'ui-toggle',
								 									]
								 ],
								 [
								   'id' 				 => 'enigma_google_plus_url', //field id
								 	'title' 		 => __('Google+',"enigma_connect_with"), //field title
								 	'callback'   =>[$this->admin, 'enigmaGooglePlus'],//function to show the field
								 	'page'    	 => 'enigma_connect_with_me',//page the field will show
								 	'section' 	 => 'enigma_admin_index',//section the field will appear
								 	'args' 			 => [
								 										'label_for'  => 'cpt_manager',
								 										'class'      => 'ui-toggle',
								 									]
								 ],
								 [
								   'id' 				 => 'enigma_pinterest_url', //field id
								 	'title' 		 => __('Pinterest', "enigma_connect_with"), //field title
								 	'callback'   =>[$this->admin, 'enigmaPinterest'],//function to show the field
								 	'page'    	 => 'enigma_connect_with_me',//page the field will show
								 	'section' 	 => 'enigma_admin_index',//section the field will appear
								 	'args' 			 => [
								 										'label_for'  => 'cpt_manager',
								 										'class'      => 'ui-toggle',
								 									]
								 ],
								 [
								   'id' 				 => 'enigma_flicker_url', //field id
								 	'title' 		 => __('Flicker', "enigma_connect_with"), //field title
									'callback'   =>[$this->admin, 'enigmaFlicker'],//function to show the field
								 	'page'    	 => 'enigma_connect_with_me',//page the field will show
							 	'section' 	 => 'enigma_admin_index',//section the field will appear
								 	'args' 			 => [
								 										'label_for'  => 'cpt_manager',
								 										'class'      => 'ui-toggle',
																]
								 ],
								[
								  'id' 				 => 'enigma_youtube_url', //field id
							'title' 		 => __('Youtube',"enigma_connect_with"), //field title
									'callback'   =>[$this->admin, 'enigmaYoutube'],//function to show the field
								 	'page'    	 => 'enigma_connect_with_me',//page the field will show
								 	'section' 	 => 'enigma_admin_index',//section the field will appear
									'args' 			 => [
																		'label_for'  => 'cpt_manager',
																	'class'      => 'ui-toggle',
																	]
								 ],
								 [
								  'id' 				 => 'enigma_twitter_url', //field id
								 	'title' 		 => __('Twitter',"enigma_connect_with"), //field title
								 	'callback'   =>[$this->admin, 'enigmaTwitter'],//function to show the field
									'page'    	 => 'enigma_connect_with_me',//page the field will show
								 	'section' 	 => 'enigma_admin_index',//section the field will appear
								 	'args' 			 => [
																		'label_for'  => 'cpt_manager',
																		'class'      => 'ui-toggle',
								 									]
								],

					 ];

		$this->settings->setFields($args);
	}



}
