<?php

/**
 *
 * This class defines all admin callbacks
 *
 * @since      1.0.0
 * @package    Enigma_Connect_With
 * @subpackage Enigma_ChrisIncludes/Api/Settings
 * @author     Christian <nwachukwu16@gmail.com>
 */
namespace Inc\Api\Settings;

class  SettingsApi
{

    public $admin_pages = array();
    public $admin_subpages = array();
    public $settings = array();
    public $sections = array();
    public $fields = array();

    public function register()
    {
        //check if the admin pages array is empty
        if(!empty($this->admin_pages))
        {
            add_action( $tag = 'admin_menu', $function_to_add = array($this, 'addAdminMenu') );
        }

        //Check if the settings array is empty
        if(!empty($this->settings))
        {
            add_action( $tag = 'admin_init', $function_to_add = [ $this, 'registerCustomFields'] );
        }

    }

    //Accepts only array of pages.
    public function addPages(array $pages)
    {
        $this->admin_pages = $pages;
        return $this;
    }



    //Create the subpages
    public function withSubPage(string $title = null){
        if(empty($this->admin_pages))
        {
            return $this;
        }

        $admin_page =$this->admin_pages[0];

        $subpage = [
                    [
                       'parent_slug' => $admin_page['menu_slug'],
                        'page_title'  => $admin_page['page_title'],
                        'menu_title' => ($title) ? $title :  $admin_page['menu_title'],
                        'capability'   => $admin_page['capability'],
                        'menu_slug'  =>  $admin_page['menu_slug'],
                        'callback'     => $admin_page['callback'],
                    ]
        ];

        //Add this to our subpages array
          $this->admin_subpages = $subpage;

          return $this;
    }
    public function addSubPages(array $pages)
    {
        $this->admin_subpages = array_merge($this->admin_subpages , $pages);

        return $this;
    }


    //Add the page menu to the list.
    public function addAdminMenu()
    {

        foreach ($this->admin_pages as $page)
        {

         add_menu_page( $page_title = $page['page_title'], $menu_title = $page['menu_title'], $capability = $page['capability'], $menu_slug = $page['menu_slug'], $function = $page['callback'], $icon_url = $page['icon_url'], $position = $page['position'] );

        }


        foreach ($this->admin_subpages as $page)
        {
         add_submenu_page( $parent_slug = $page['parent_slug'], $page_title = $page['page_title'], $menu_title = $page['menu_title'], $capability = $page['capability'], $menu_slug = $page['menu_slug'], $function = $page['callback']);
        }

    }



    /*
      Series of methods that are used to create the necessary custom field pages
    */

    public function setSettings(array $settings)
    {
        $this->settings = $settings;
        return $this;
    }

    public function setSections(array $sections)
    {
        $this->sections = $sections;
        return $this;
    }

    public function setFields(array $fields)
    {
        $this->fields = $fields;
        return $this;
    }

  // Registering some admin custom field dynamically
    public function registerCustomFields(){

          //register setting
            foreach ($this->settings as $setting)
            {
                  register_setting( $setting['option_group'],  $setting['option_name'],   (isset( $setting['callback']) ?  $setting['callback'] : '' ));
           }

            //add setting section
            foreach ($this->sections as $section)
            {
                add_settings_section( $section['id'],  $section['title'],  (isset( $section['callback']) ?  $section['callback'] : '' ),  $section['page'] );
           }
            //add settings field
            foreach ($this->fields as $field)
            {
                add_settings_field( $field['id'], $field['title'],  (isset( $field['callback']) ?  $field['callback'] : '' ), $field['page'],  $field['section'],  (isset( $field['args']) ?  $field['args'] : '' ));
           }

    }



}
