<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       https://enigma-chris.ca
 * @since      1.0.0
 *
 * @package    Enigma_Chris
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
//clear database database

    //Delete the option colors
    delete_option( 'enigma_facebook_color' );
    delete_option( 'enigma_twitter_color' );
    delete_option( 'enigma_linkedin_color' );
    delete_option( 'enigma_instagram_color' );
    delete_option( 'enigma_google_plus_color' );
    delete_option( 'enigma_pinterest_color' );
    delete_option( 'enigma_flicker_color' );
    delete_option( 'enigma_youtube_color' );
    delete_option( 'enigma_title_color' );

    //delete the sizes
    delete_option( 'enigma_icon_size' );
    delete_option( 'enigma_title_size' );

    //Delete the social urls
    delete_option( 'enigma_facebook_url' );
    delete_option( 'enigma_twitter_url' );
    delete_option( 'enigma_linkedin_url' );
    delete_option( 'enigma_instagram_url' );
    delete_option( 'enigma_google_plus_url' );
    delete_option( 'enigma_pinterest_url' );
    delete_option( 'enigma_flicker_url' );
    delete_option( 'enigma_youtube_url' );
    delete_option( 'enigma_title_url' );
