<?php
  echo _e("<h1>Socials Icon Size Settings</h1>", 'enigma_connect_with');
  settings_errors(); ?>
  <div id="manage-settings" class="tab-pane fade in active">
    <form class="" action="options.php" method="post">
        <?php
        settings_fields( $option_group = 'enigma_connect_with_me_size_settings' );
        do_settings_sections( $page = 'enigma_icon_size' );
        submit_button();
        ?>
    </form>
  </div>
