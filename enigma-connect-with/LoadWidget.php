<?php

/**
 * Fired during plugin activation
 *
 * @link       https://enigma-chris.ca
 * @since      1.0.0
 *
 * @package    Enigma_Chris
 * @subpackage Enigma_Chris/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Enigma_Chris
 * @subpackage Enigma_Chris/includes
 * @author     Christian <nwachukwu16@gmail.com>
 */
//


class LoadWidget extends WP_Widget
{

	public function register(){

	}

	public function __construct(){
			parent::__construct(false, $name = __('Social Settings', 'enigma_connect_with'));
	}


public function form( $instance ) {

  $title = ! empty( $instance['title'] ) ? $instance['title'] : '';

	$facebook = '';
  if( !empty( $instance['facebook'] ) ) {
      $facebook = $instance['facebook'];
  }

	$linkedin = '';
  if( !empty( $instance['linkedin'] ) ) {
      $linkedin = $instance['linkedin'];
  }

	$instagram = '';
  if( !empty( $instance['instagram'] ) ) {
      $instagram = $instance['instagram'];
  }
	$googleplus = '';
  if( !empty( $instance['googleplus'] ) ) {
      $googleplus = $instance['googleplus'];
  }
	$pinterest = '';
  if( !empty( $instance['pinterest'] ) ) {
      $pinterest = $instance['pinterest'];
  }
	$flicker = '';
  if( !empty( $instance['flicker'] ) ) {
      $flicker = $instance['flicker'];
  }
	$twitter= '';
  if( !empty( $instance['twitter'] ) ) {
      $twitter = $instance['twitter'];
  }
	$youtube = '';
  if( !empty( $instance['youtube'] ) ) {
      $youtube = $instance['youtube'];
  }

?>
  <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
    <input type="text" class="widefat title" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php _e( esc_attr( $title ), "enigma_connect_with"); ?>" />
  </p>

	<p>
		<label for="connect-on"><?php _e( 'Connect on:' ); ?></label><br/>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="facebook" <?php echo ($facebook == 'facebook') ? "checked" : ''; ?>> <?php _e('Facebook',"enigma_connect_with"); ?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="linkedin" <?php echo ($linkedin== 'linkedin') ? "checked" : ''; ?>>  <?php _e('Linkedin', "enigma_connect_with");?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'instagram' ); ?>" value="instagram" <?php echo ($instagram == 'instagram') ? "checked" : ''; ?>> <?php _e('Instagram', "enigma_connect_with");?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'googleplus' ); ?>" value="googleplus" <?php echo ($googleplus == 'googleplus') ? "checked" : ''; ?>> <?php _e('Google+',"enigma_connect_with"); ?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" value="pinterest" <?php echo ($pinterest == 'pinterest') ? "checked" : ''; ?>> <?php _e('Pinterest', "enigma_connect_with");?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'flicker' ); ?>" value="flicker" <?php echo ($flicker == 'flicker') ? "checked" : ''; ?>> <?php _e('Flicker', "enigma_connect_with");?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="twitter" <?php echo ($twitter == 'twitter') ? "checked" : ''; ?>> <?php _e('Twitter',"enigma_connect_with"); ?><br>
		<input type="checkbox" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="youtube" <?php echo ($youtube == 'youtube') ? "checked" : ''; ?>> <?php _e('Youtube', "enigma_connect_with");?><br>
	</p>

	<?php

}


public function update( $new_instance, $old_instance ) {
  $instance = $old_instance;
  $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );

	$instance[ 'facebook' ] 	= strip_tags( $new_instance[ 'facebook' ] );
	$instance[ 'linkedin' ] 	= strip_tags( $new_instance[ 'linkedin' ] );
	$instance[ 'instagram' ] 	= strip_tags( $new_instance[ 'instagram' ] );
	$instance[ 'googleplus' ] = strip_tags( $new_instance[ 'googleplus' ] );
	$instance[ 'pinterest' ] 	= strip_tags( $new_instance[ 'pinterest' ] );
	$instance[ 'flicker' ] 		= strip_tags( $new_instance[ 'flicker' ] );
	$instance[ 'twitter' ] 		= strip_tags( $new_instance[ 'twitter' ] );
	$instance[ 'youtube' ] 		= strip_tags( $new_instance[ 'youtube' ] );
  return $instance;
}


	public function widget( $args, $instance ) {

		?>

		<style>
				div.social-plugin a.social-link{
					text-decoration: none !important;
					font-size: <?php echo get_option('enigma_icon_size' )."px;"; ?>  !important;
					margin-right: 7px;
					border-bottom: none;
				}

				div.social-plugin .icon-title{
					font-size: <?php echo esc_attr( get_option( $option = 'enigma_title_size', $default = false ) )."px;" ?>
				}
		</style>

		<?php



  $title = apply_filters( 'widget_title', $instance[ 'title' ] );

	$response = '<h4 class="icon-title" style="color:'.get_option( $option = 'enigma_title_color').'" >'.$title.'</h4>';

  if( !empty( $instance['facebook'] ) ) {
		if( !empty(get_option( $option = 'enigma_facebook_url')) ){
			$response.= '<a class="social-link"  style="color:'.get_option( $option = 'enigma_facebook_color').'"  href="'.get_option( $option = 'enigma_facebook_url').'"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>';
		}
	}

  if( !empty( $instance['linkedin'] ) ) {
		if( !empty(get_option( $option = 'enigma_linkedin_url')) ){
		$response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_linkedin_color').'" href="'.get_option( $option = 'enigma_linkedin_url').'"> <i class="fa fa-linkedin" aria-hidden="true"></i></a>';
  	}
	}

  if( !empty( $instance['instagram'] ) ) {
		if( !empty(get_option( $option = 'enigma_linkedin_url')) ){
		$response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_instagram_color').'" href="'.get_option( $option = 'enigma_instagram_url').'"> <i class="fa fa-instagram" aria-hidden="true"></i>';
		}
	}
  if( !empty( $instance['googleplus'] ) ) {
		if( !empty(get_option( $option = 'enigma_google_plus_url')) ){
			$response.= ' <a class="social-link" style="color:'.get_option( $option = 'enigma_google_plus_color').'" href="'.get_option( $option = 'enigma_google_plus_url').'"> <i class="fa fa-google-plus" aria-hidden="true"></i> ';
		}
	}
  if( !empty( $instance['pinterest'] ) ) {
		if( !empty(get_option( $option = 'enigma_pinterest_url')) ){
			$response.= ' <a class="social-link" style="color:'.get_option( $option = 'enigma_pinterest_color').'" href="'.get_option( $option = 'enigma_pinterest_url').'"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a>';
	 	}
  }
  if( !empty( $instance['flicker'] ) ) {
		if( !empty(get_option( $option = 'enigma_flicker_url')) ){
			$response.= ' <a class="social-link" style="color:'.get_option( $option = 'enigma_flicker_color').'" href="'.get_option( $option = 'enigma_flicker_url').'"> <i class="fa fa-flickr" aria-hidden="true"></i> </a>';
		}
  }
  if( !empty( $instance['twitter'] ) ) {
		if( !empty(get_option( $option = 'enigma_twitter_url')) ){
		$response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_twitter_color').'" href="'.get_option( $option = 'enigma_twitter_url').'"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>';
		}
	}
  if( !empty( $instance['youtube'] ) ) {
		if( !empty(get_option( $option = 'enigma_youtube_url')) ){
		$response.= '  <a class="social-link" style="color:'.get_option( $option = 'enigma_youtube_color').'" href="'.get_option( $option = 'enigma_youtube_url').'"> <i class="fa fa-youtube" aria-hidden="true"></i> </a>';
		}
	}


	$output = '<div class="social-plugin">'.$response. '</div>';
  echo $args['before_widget'] . $args['before_title'] . $output . $args['after_title']; ?>


  <?php echo $args['after_widget'];

}

}

add_action('widgets_init', function(){
	register_widget( $widget_class = 'LoadWidget' );
});

?>
